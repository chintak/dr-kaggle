# Prepare training images.
from __future__ import division
import numpy as np
import pandas as pd
from pandas import read_csv
from skimage.io import imsave
import argparse
import caffe
import cv2
import os

from utils import parse_folder, image_load, make_folder_tree
from utils import extract_filename_in_path, text_number_list_to_int_list
from preprocessing import preproc_tight_bb_crop10p

from joblib import Parallel, delayed


def preproc_multiple_scales(input_file_name, label, output_folders,
                            scales, i, tot):
    if i % 10 == 0:
        print "Processed - %d/%d" % (i, tot)
    img = image_load(input_file_name)
    lab = ""
    if label != '':
        lab = '%d' % (label)
    name = "%s.jpeg" % extract_filename_in_path(input_file_name)
    for scale, out_folder in zip(scales, output_folders):
        try:
            out_img = preproc_tight_bb_crop10p(img, scale)
            out_name = os.path.join(out_folder, lab, name)
            make_folder_tree(out_name)
            imsave(out_name, out_img)
        except Exception, e:
            print e
            print "\n\nError while processing %s" % input_file_name
            with open('err.log', 'a') as f:
                f.write("%s\n" % input_file_name)
                f.close()


def sample_train_val_split(names, pdlab, valsplit):
    pdlab = pdlab.reset_index()
    counts = pd.Series(pdlab.label).value_counts()
    samples_per_label = {}
    total_num = pdlab.count()['label']
    val_idx = np.zeros(total_num, dtype=np.uint8)
    rng = np.random.RandomState(1234)
    for k in counts.keys():
        samples_per_label[k] = int(round(valsplit * counts[k] / total_num))
        b = np.arange(counts[k])
        rng.shuffle(b)
        b = b[:samples_per_label[k]]
        val_idx[pdlab.index[pdlab.label == k][b]] = 1
    pdlab['val'] = pd.Series(val_idx, index=pdlab.index)
    pdlab = pdlab.set_index('image')
    return pdlab


def main(orig_train_folder, labels_file, orig_test_folder,
         out_folder_parent, valsplit, scales):
    train = True if orig_train_folder is not None else False
    test = True if orig_test_folder is not None else False

    print "Reading images from %s" % orig_train_folder
    # Parse train dataset folder
    if train:
        names = parse_folder(orig_train_folder, "jpeg")
        print "Total number of train files: %d" % len(names)
        # Read the training image labels
        pdlab = read_csv(labels_file,
                         names=['image', 'label'], index_col='image', header=0)
        # Determine the train and val split
        pdlab = sample_train_val_split(names, pdlab, valsplit)
        print "Number of validation images selected: %d" % (
            pdlab.index[pdlab.val == 1].shape[0])
    # Parse test dataset folder
    if test:
        test_names = parse_folder(orig_test_folder, "jpeg")
        print "Total number of test files: %d" % len(test_names)
    # Create all the output folders
    train_output_folders = []
    val_output_folders = []
    test_output_folders = []
    for scale in scales:
        train_output_folders.append(os.path.join(
            out_folder_parent, "%d_train" % (2 * scale)))
        val_output_folders.append(os.path.join(
            out_folder_parent, "%d_val" % (2 * scale)))
        test_output_folders.append(os.path.join(
            out_folder_parent, "%d_test" % (2 * scale)))
    # Create a parallel pool
    with Parallel(n_jobs=8) as parallel:
        # Processing train/val images
        if train:
            rets = parallel(delayed(preproc_multiple_scales)(
                fname,
                pdlab.ix[extract_filename_in_path(fname)]['label'],
                train_output_folders if pdlab.ix[
                    extract_filename_in_path(fname)][
                    'val'] == 0 else val_output_folders,
                scales,
                i,
                len(names))
                for i, fname in enumerate(names[::-1]))
        # Processing test images
        if test:
            rets = parallel(delayed(preproc_multiple_scales)(
                fname,
                "",
                test_output_folders,
                scales)
                for fname in test_names)
        print "Done. A total of %d files processed." % len(rets)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-orig",
                        help="original train dataset root folder.",
                        type=str)
    parser.add_argument("-test",
                        help="original test dataset root folder.",
                        type=str)
    parser.add_argument("-lab",
                        help="train labels csv path.",
                        type=str)
    parser.add_argument("-out",
                        help="output train, val & test dataset parent folder.",
                        type=str)
    parser.add_argument("-valsplit",
                        type=int,
                        default=0,
                        help="Number of images in validation set.")
    parser.add_argument("-scales",
                        type=str,
                        default=0,
                        help="Scale of processing for the input images."
                        " -scale 100,128,150")

    args = parser.parse_args()
    scales = text_number_list_to_int_list(args.scales, ',')
    main(args.orig, args.lab, args.test, args.out, args.valsplit, scales)
