# Prepare training images.
from __future__ import division
import cv2
import numpy as np
import os
import caffe
from utils import scale_radius, bbox, pad_img


def subsample_inner_circle_img(a, scale):
    b = np.zeros(a.shape)
    cv2.circle(b, (a.shape[1] // 2, a.shape[0] // 2),
               int(scale * 0.9), (1, 1, 1), -1, 8, 0)
    aa = a * b
    aa = aa.astype(np.uint8)
    return aa


def preproc_tight_bb_crop10p(img, scale):
    """Crop the extra black region, outer 10% of
    the retinal circle and 10 pixels from top and bottom
    of the image.

    Return
        out: shape (2 * scale, 2 * scale, 3)
    """
    simg = scale_radius(img, round(scale / .9))
    suimg = subsample_inner_circle_img(simg, round(scale / .9))
    cimg = bbox(suimg)
    pimg = pad_img(cimg, (2 * scale, 2 * scale, 3))
    pimg[:10, :, :] = 0
    pimg[-10:, :, :] = 0
    return pimg

if __name__ == '__main__':
    pass
