import argparse


def get():
    """Argument parser."""

    parser = argparse.ArgumentParser()
    parser.add_argument("-mode",
                        "--mode",
                        type=str,
                        default="train",
                        help=""
                        "'train' performs training on --train-dataset (default)\n"
                        "'test' performs testing on --test-dataset\n")

    parser.add_argument("-t",
                        "--train-dataset",
                        type=str,
                        default="data/256_train/")
    parser.add_argument("-v",
                        "--validation-dataset",
                        type=str,
                        default="data/256_val/")
    args = parser.parse_args()
    return args if arguments_sanity_check(args) is True


def arguments_sanity_check(args):
    if args.mode not in {'train', 'test'}:
        raise ValueError(
            "arguments.py: Invalid value %s for argument -mode."
            " Use -h for help." % args.mode)

    return True
